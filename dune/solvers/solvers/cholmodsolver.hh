// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_SOLVERS_CHOLMODSOLVER_HH
#define DUNE_SOLVERS_SOLVERS_CHOLMODSOLVER_HH

/** \file
    \brief A wrapper for the CHOLMOD sparse direct solver
*/

#include <dune/common/exceptions.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/solver.hh>
#include <dune/istl/cholmod.hh>
#include <dune/istl/foreach.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/common/canignore.hh>
#include <dune/solvers/solvers/linearsolver.hh>
#include <dune/solvers/common/defaultbitvector.hh>

namespace Dune
{

namespace Solvers
{

/** \brief Wraps the CHOLMOD sparse symmetric direct solver
*
*  CHOLMOD uses the Cholesky decomposition A = LL^T for sparse
*  symmetric matrices to solve linear systems Ax = b efficiently.
*
*  This class wraps the dune-istl CHOLMOD solver for dune-solvers to provide
*  a Solvers::LinearSolver interface.
*/
template <class MatrixType, class VectorType, class BitVectorType = DefaultBitVector_t<VectorType>>
class CholmodSolver
: public LinearSolver<MatrixType,VectorType>, public CanIgnore<BitVectorType>
{
  using field_type = typename VectorType::field_type;

public:

  /** \brief Default constructor */
  CholmodSolver ()
  : LinearSolver<MatrixType,VectorType>(NumProc::FULL)
  {}

  /** \brief Constructor for a linear problem */
  CholmodSolver (const MatrixType& matrix,
                 VectorType& x,
                 const VectorType& rhs,
                 NumProc::VerbosityMode verbosity=NumProc::FULL)
  : LinearSolver<MatrixType,VectorType>(verbosity),
    matrix_(&matrix),
    x_(&x),
    rhs_(&rhs)
  {}

  /** \brief Set the linear problem to solve
   *
   * Warning! The matrix is assumed to be symmetric!
   * CHOLMOD uses only the upper part of the matrix, the lower part is ignored and can be empty for optimal memory usage.
   */
  void setProblem(const MatrixType& matrix,
                  VectorType& x,
                  const VectorType& rhs) override
  {
    matrix_ = &matrix;
    x_ = &x;
    rhs_ = &rhs;
  }

  virtual void solve()
  {
    // prepare the solver
    Dune::InverseOperatorResult statistics;
    Dune::Cholmod<VectorType> solver;

    // Bug in LibScotchMetis:
    // metis_dswitch: see cholmod.h: LibScotchMetis throws segmentation faults for large problems.
    // This caused a hard to understand CI problems and therefore, METIS is disabled for problems
    // larger than defined in metis_nswitch for now (default 3000).
    // Bug report: https://gitlab.inria.fr/scotch/scotch/issues/8
    solver.cholmodCommonObject().metis_dswitch = 0.0;

    if (not this->hasIgnore())
    {
      // the apply() method doesn't like constant values
      auto mutableRhs = *rhs_;

      // setMatrix will decompose the matrix
      solver.setMatrix(*matrix_);
      // get the error code from Cholmod in case something happened
      errorCode_ = solver.cholmodCommonObject().status;

      solver.apply(*x_, mutableRhs, statistics);
    }
    else
    {
      const auto& ignore = this->ignore();

      // The setMatrix method stores only the selected entries of the matrix (determined by the ignore field)
      solver.setMatrix(*matrix_,&ignore);
      // get the error code from Cholmod in case something happened
      errorCode_ = solver.cholmodCommonObject().status;

      // create flat vectors
      using T = typename VectorType::field_type;
      std::size_t flatSize = flatVectorForEach(ignore, [](...){});
      std::vector<bool> flatIgnore(flatSize);
      std::vector<T> flatX(flatSize), flatRhsModifier(flatSize);

      // define a lambda that copies the entries of a blocked vector into a flat one
      auto copyToFlat = [&](auto&& blocked, auto&& flat)
      {
        flatVectorForEach(blocked, [&](auto&& entry, auto&& offset)
        {
          flat[offset] = entry;
        });
      };

      // copy x and the ignore field for the modification of the rhs
      copyToFlat(ignore,flatIgnore);
      copyToFlat(*x_,flatX);

      flatMatrixForEach( *matrix_, [&]( auto&& entry, auto&& rowOffset, auto&& colOffset){

        // we assume entries only in the upper part and do nothing on the diagonal
        if ( rowOffset >= colOffset )
          return;

        // upper part: if either col or row is ignored: move the entry or the symmetric duplicate to the rhs
        if ( flatIgnore[colOffset] and not flatIgnore[rowOffset] )
            flatRhsModifier[rowOffset] += entry * flatX[colOffset];

        else if ( not flatIgnore[colOffset] and flatIgnore[rowOffset] )
            flatRhsModifier[colOffset] += entry * flatX[rowOffset];
      });

      // update the rhs
      auto modifiedRhs = *rhs_;
      flatVectorForEach(modifiedRhs, [&](auto&& entry, auto&& offset){
        if ( not flatIgnore[offset] )
          entry -= flatRhsModifier[offset];
      });

      // Solve the modified system
      solver.apply(*x_, modifiedRhs, statistics);
    }
  }

  /** \brief return the error code of the Cholmod factorize call
   *
   * In setMatrix() the matrix factorization takes place and Cholmod is
   * able to communicate error codes of the factorization in the status
   * field of the cholmodCommonObject.
   * The return value 0 means "good" and the other values can be found
   * in the Cholmod User Guide.
   */
  int errorCode() const
  {
    return errorCode_;
  }

  //! Pointer to the system matrix
  const MatrixType* matrix_;

  //! Pointer to the solution vector
  VectorType* x_;

  //! Pointer to the right hand side
  const VectorType* rhs_;

  //! error code of Cholmod factorize call
  int errorCode_ = 0;
};

}   // namespace Solvers
}   // namespace Dune

#endif
