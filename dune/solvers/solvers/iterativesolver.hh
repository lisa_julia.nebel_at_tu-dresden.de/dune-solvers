// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIVE_SOLVER_HH
#define DUNE_SOLVERS_ITERATIVE_SOLVER_HH

#include <dune/common/ftraits.hh>
#include <dune/common/shared_ptr.hh>

#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/common/wrapownshare.hh>
#include <dune/solvers/solvers/solver.hh>
#include <dune/solvers/iterationsteps/iterationstep.hh>
#include <dune/solvers/norms/norm.hh>

namespace Dune {

  namespace Solvers {

    /** \brief Abstract base class for iterative solvers */
    template <class VectorType, class BitVectorType = DefaultBitVector_t<VectorType> >
    class IterativeSolver : public Solver
    {
        // For norms and convergence rates
        using real_type =  typename Dune::FieldTraits<VectorType>::real_type;
        using ItStep = IterationStep<VectorType, BitVectorType>;
        using ErrorNorm = Norm<VectorType>;

    public:

        /** \brief Constructor taking all relevant data */
        IterativeSolver(double tolerance,
                        int maxIterations,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations), errorNorm_(nullptr),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Constructor taking all relevant data */
        DUNE_DEPRECATED_MSG("Handing over raw pointer in the constructor is deprecated!")
        IterativeSolver(int maxIterations,
                        double tolerance,
                        const Norm<VectorType>* errorNorm,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations),
              errorNorm_(Dune::stackobject_to_shared_ptr(*errorNorm)),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Constructor taking all relevant data */
        template <class NormT, class Enable = std::enable_if_t<not std::is_pointer<NormT>::value> >
        IterativeSolver(int maxIterations,
                        double tolerance,
                        NormT&& errorNorm,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations),
              errorNorm_(wrap_own_share<const ErrorNorm>(std::forward<NormT>(errorNorm))),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Loop, call the iteration procedure
         * and monitor convergence */
        virtual void solve() = 0;

        /** \brief Checks whether all relevant member variables are set
         */
        virtual void check() const;

        /** \brief Write the current iterate to disk (for convergence measurements) */
        void writeIterate(const VectorType& iterate, int iterationNumber) const;

        /** \brief Set iteration step */
        template <class Step>
        void setIterationStep(Step&& iterationStep)
        {
            iterationStep_ = wrap_own_share<ItStep>(std::forward<Step>(iterationStep));
        }

        /** \brief Get iteration step */
        const ItStep& getIterationStep() const
        {
          return *iterationStep_;
        }

        /** \brief Get iteration step */
        ItStep& getIterationStep()
        {
          return *iterationStep_;
        }

        /** \brief Set the error norm */
        template <class NormT>
        void setErrorNorm(NormT&& errorNorm)
        {
            errorNorm_ = wrap_own_share<ErrorNorm>(std::forward<NormT>(errorNorm));
        }

        /** \brief Get error norm */
        const ErrorNorm& getErrorNorm() const
        {
          return *errorNorm_;
        }

        /** \brief The requested tolerance of the solver */
        double tolerance_;

        //! The maximum number of iterations
        int maxIterations_;

    protected:
        //! The iteration step used by the algorithm
        std::shared_ptr<ItStep> iterationStep_;

        //! The norm used to measure convergence
        std::shared_ptr<const ErrorNorm> errorNorm_;

    public:
        /** \brief If this string is nonempty it is expected to contain a valid
            directory name.  All intermediate iterates are then written there. */
        std::string historyBuffer_;

        real_type maxTotalConvRate_;

        bool useRelativeError_;

    };

  }   // namespace Solvers

}   // namespace Dune

// For backward compatibility: will be removed eventually
using Dune::Solvers::IterativeSolver;

#include "iterativesolver.cc"

#endif
