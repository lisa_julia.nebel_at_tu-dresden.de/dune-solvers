// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_SOLVERS_UMFPACKSOLVER_HH
#define DUNE_SOLVERS_SOLVERS_UMFPACKSOLVER_HH

/** \file
    \brief A wrapper for the UMFPack sparse direct solver
*/
#include <algorithm>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/solver.hh>
#include <dune/istl/umfpack.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/common/canignore.hh>
#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/solvers/linearsolver.hh>

namespace Dune
{

namespace Solvers
{

/** \brief Wraps the UMFPack sparse direct solver */
template <class MatrixType, class VectorType>
class UMFPackSolver
: public LinearSolver<MatrixType,VectorType>, public CanIgnore<DefaultBitVector_t<VectorType> >
{
public:

  /** \brief Default constructor */
  UMFPackSolver ()
  : LinearSolver<MatrixType,VectorType>(NumProc::FULL)
  {}

  /** \brief Constructor for a linear problem */
  UMFPackSolver (const MatrixType& matrix,
                 VectorType& x,
                 const VectorType& rhs,
                 NumProc::VerbosityMode verbosity=NumProc::FULL)
  : LinearSolver<MatrixType,VectorType>(verbosity),
    matrix_(&matrix),
    x_(&x),
    rhs_(&rhs)
  {}

  void setProblem(const MatrixType& matrix,
                  VectorType& x,
                  const VectorType& rhs) override
  {
    matrix_ = &matrix;
    x_ = &x;
    rhs_ = &rhs;
  }

  void solve() override
  {
    // We may use the original rhs, but ISTL modifies it, so we need a non-const type here
    VectorType mutableRhs = *rhs_;

    if (not this->hasIgnore())
    {
      /////////////////////////////////////////////////////////////////
      //  Solve the system
      /////////////////////////////////////////////////////////////////
      Dune::InverseOperatorResult statistics;
      Dune::UMFPack<MatrixType> solver(*matrix_);
      solver.setOption(UMFPACK_PRL, 0);   // no output at all
      solver.apply(*x_, mutableRhs, statistics);

    }
    else
    {
      ///////////////////////////////////////////////////////////////////////////////////////////
      //  Extract the set of matrix rows that do not correspond to ignored degrees of freedom.
      //  Unfortunately, not all cases are handled by the ISTL UMFPack solver.  Currently,
      //  you can only remove complete block rows.  If a block is only partially ignored,
      //  the dune-istl UMFPack solver cannot do it, and the code here will throw an exception.
      //  All this can be fixed, but it needs going into the istl UMFPack code.
      ///////////////////////////////////////////////////////////////////////////////////////////
      std::set<std::size_t> nonIgnoreRows;
      for (size_t i=0; i<matrix_->N(); i++)
      {
        auto const &ignore = this->ignore()[i];
        if constexpr (std::is_convertible<decltype(ignore), bool>::value)
        {
          if (!ignore)
            nonIgnoreRows.insert(i);
        } else {
          if (ignore.none())
            nonIgnoreRows.insert(i);
          else if (not ignore.all())
            DUNE_THROW(Dune::NotImplemented, "Individual blocks must be either ignored completely, or not at all");
        }
      }

      // Construct the solver
      Dune::InverseOperatorResult statistics;
      Dune::UMFPack<MatrixType> solver;
      solver.setOption(UMFPACK_PRL, 0);   // no output at all
      // We eliminate all rows and columns(!) from the matrix that correspond to ignored degrees of freedom.
      // Here is where the sparse LU decomposition is happenening.
      solver.setSubMatrix(*matrix_,nonIgnoreRows);

      // We need to modify the rhs vector by static condensation.
      VectorType shortRhs(nonIgnoreRows.size());
      int shortRowCount=0;
      for (auto it = nonIgnoreRows.begin(); it!=nonIgnoreRows.end(); ++shortRowCount, ++it)
        shortRhs[shortRowCount] = mutableRhs[*it];

      shortRowCount = 0;
      for (size_t i=0; i<matrix_->N(); i++)
      {
        if constexpr (std::is_convertible<decltype(this->ignore()[i]), const bool>::value) {
          if (this->ignore()[i])
            continue;
        } else {
          if (this->ignore()[i].all())
            continue;
        }

        auto cIt    = (*matrix_)[i].begin();
        auto cEndIt = (*matrix_)[i].end();

        for (; cIt!=cEndIt; ++cIt)
          if constexpr (std::is_convertible<decltype(this->ignore()[cIt.index()]), const bool>::value) {
            if (this->ignore()[cIt.index()])
              Dune::Impl::asMatrix(*cIt).mmv((*x_)[cIt.index()], shortRhs[shortRowCount]);
          } else {
            if (this->ignore()[cIt.index()].all())
              Dune::Impl::asMatrix(*cIt).mmv((*x_)[cIt.index()], shortRhs[shortRowCount]);
          }

        shortRowCount++;
      }

      // Solve the reduced system
      VectorType shortX(nonIgnoreRows.size());
      solver.apply(shortX, shortRhs, statistics);

      // Blow up the solution vector
      shortRowCount=0;
      for (auto it = nonIgnoreRows.begin(); it!=nonIgnoreRows.end(); ++shortRowCount, ++it)
        (*x_)[*it] = shortX[shortRowCount];

    }
  }

  ///////////////////////////////////////////////////////
  // Data
  ///////////////////////////////////////////////////////

  //! The quadratic term in the quadratic energy
  const MatrixType* matrix_;

  //! Vector to store the solution
  VectorType* x_;

  //! The linear term in the quadratic energy
  const VectorType* rhs_;
};

}   // namespace Solvers

}   // namespace Dune

#endif
