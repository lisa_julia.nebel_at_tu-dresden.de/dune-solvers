// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_PROJECTED_LINE_GAUSS_SEIDEL_STEP_HH
#define DUNE_PROJECTED_LINE_GAUSS_SEIDEL_STEP_HH

#include <dune/common/bitsetvector.hh>

#include <dune/istl/btdmatrix.hh>

#include <dune/solvers/common/boxconstraint.hh>
#include <dune/solvers/iterationsteps/linegsstep.hh>

template<class MatrixType,
         class VectorType,
         class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
class ProjectedLineGSStep : public LineGSStep<MatrixType, VectorType, BitVectorType>
{

    typedef typename VectorType::block_type VectorBlock;

    typedef typename VectorType::field_type field_type;

    enum {BlockSize = VectorBlock::dimension};

public:

    //! Default constructor.  Doesn't init anything
    ProjectedLineGSStep() {}

    //! Constructor for usage of Permutation Manager
    ProjectedLineGSStep( const std::vector<std::vector<unsigned int> >& blockStructure)
        : LineGSStep<MatrixType, VectorType, BitVectorType>( blockStructure )
    {}

    /** \brief Solve one tridiagonal system */
    void solveLocalSystem(const Dune::BTDMatrix<typename MatrixType::block_type>& matrix,
                          VectorType& x,
                          const VectorType& rhs,
                          const std::vector<BoxConstraint<field_type,BlockSize> >& localObstacles) const;

    //! Perform one iteration
    virtual void iterate();

    const std::vector<BoxConstraint<field_type,BlockSize> >* obstacles_;

};


#include "projectedlinegsstep.cc"

#endif
