// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_TRANSFEROPERATORS_MANDEL_OBSTACLE_RESTRICTOR_HH
#define DUNE_SOLVERS_TRANSFEROPERATORS_MANDEL_OBSTACLE_RESTRICTOR_HH

#include <dune/common/bitsetvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/solvers/transferoperators/obstaclerestrictor.hh>
#include <dune/solvers/transferoperators/multigridtransfer.hh>
#include <dune/solvers/common/boxconstraint.hh>

/** \brief Mandel restriction to compute coarse grid obstacles, needed within montone multigrid methods.
 *
 * The Mandel restriction computes the maximum $m$ of all nodal base coefficients
 * in the support of a given coarse grid base function \f$ \phi_c \f$.  If the
 * coefficient of \f$ \phi_c \f$ is smaller than \f$ m \f$ , it is set to \f$ m \f$.
 *
 * \tparam VectorType The block vector type of the algebraic problem.
 */
template <class VectorType>
class MandelObstacleRestrictor : public ObstacleRestrictor<VectorType>
{
    typedef typename VectorType::field_type field_type;

    enum {blocksize = VectorType::block_type::dimension};

public:

    //! Compute coarse grid obstacles using the mandel restriction
    virtual void restrict(const std::vector<BoxConstraint<field_type,blocksize> >& f,
                          std::vector<BoxConstraint<field_type,blocksize> >& t,
                          const Dune::BitSetVector<blocksize>& fHasObstacle,
                          const Dune::BitSetVector<blocksize>& tHasObstacle,
                          const MultigridTransfer<VectorType>& transfer,
                          const Dune::BitSetVector<blocksize>& critical);

private:
    //! Helper method that allows to use the restrictor for both compressed and cense transfer operator
    template <int dim>
    void restrict(const std::vector<BoxConstraint<field_type,blocksize> >& f,
                          std::vector<BoxConstraint<field_type,blocksize> >& t,
                          const Dune::BitSetVector<blocksize>& fHasObstacle,
                          const Dune::BitSetVector<blocksize>& tHasObstacle,
                          const Dune::BCRSMatrix<Dune::FieldMatrix<field_type,dim,dim> >& transferMat,
                          const Dune::BitSetVector<blocksize>& critical);
};

// include implementation
#include "mandelobsrestrictor.cc"

#endif
