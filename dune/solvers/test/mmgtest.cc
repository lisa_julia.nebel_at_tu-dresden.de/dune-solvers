// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <array>
#include <iostream>
#include <sstream>

// dune-common includes
#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>

// dune-istl includes
#include <dune/istl/bcrsmatrix.hh>

// dune-grid includes
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/io/file/vtk/vtkwriter.hh>

// dune-solver includes
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/iterationsteps/mmgstep.hh>
#include <dune/solvers/transferoperators/mandelobsrestrictor.hh>
#include <dune/solvers/transferoperators/truncatedcompressedmgtransfer.hh>


#include "common.hh"

template<class GridType, class MatrixType, class VectorType, class BitVector>
void solveObstacleProblemByMMGSolver(const GridType& grid, const MatrixType& mat, VectorType& x, const VectorType& rhs, const BitVector& ignore, int maxIterations=100, double tolerance=1.0e-10)
{
    // create double obstacle constraints
    const int blockSize = VectorType::block_type::dimension;

    /////////////////////////////////////////////////////////////
    //  Create a monotone multigrid solver
    /////////////////////////////////////////////////////////////

    // First create a Gauss-seidel base solver
    ProjectedBlockGSStep<MatrixType, VectorType> baseSolverStep;

    EnergyNorm<MatrixType, VectorType> baseEnergyNorm(baseSolverStep);

    ::LoopSolver<VectorType> baseSolver(baseSolverStep,
                                         100,   // base iterations
                                         1e-8,  // tolerance
                                         baseEnergyNorm,
                                         Solver::QUIET);

    // Make pre and postsmoothers

    MonotoneMGStep<MatrixType, VectorType> mmgStep;

    mmgStep.setProblem(mat,x,rhs);
    mmgStep.setMGType(1, 3, 3);
    mmgStep.setIgnore(ignore);
    mmgStep.setBaseSolver(baseSolver);
    mmgStep.setSmoother(ProjectedBlockGSStep<MatrixType, VectorType>{});
    mmgStep.setObstacleRestrictor(MandelObstacleRestrictor<VectorType>());

    mmgStep.setHasObstacles(Dune::BitSetVector<blockSize>(x.size(),true));

    std::vector<BoxConstraint<typename VectorType::field_type,blockSize> > boxConstraints(rhs.size());
    for (size_t i = 0; i < boxConstraints.size(); ++i)
        for (int j = 0; j < blockSize; ++j)
            boxConstraints[i][j] = { -1, +1 };
    mmgStep.setObstacles(boxConstraints);

    // we need a vector of pointers to the transfer operator base class
    using Transfer = TruncatedCompressedMGTransfer<VectorType>;
    std::vector<std::shared_ptr<Transfer> > transfer(grid.maxLevel());
    for (size_t i = 0; i < transfer.size(); ++i)
    {
        // create transfer operator from level i to i+1
        transfer[i] = std::make_shared<Transfer>();
        transfer[i]->setup(grid, i, i+1);
    }
    mmgStep.setTransferOperators(transfer);

    mmgStep.setVerbosity(Solver::QUIET);

    EnergyNorm<MatrixType,VectorType> h1SemiNorm(mat);

    ::LoopSolver<VectorType> solver (mmgStep,
                                   maxIterations,    // iterations
                                   tolerance,   // tolerance
                                   h1SemiNorm,
                                   Solver::REDUCED);
    // solve problem
    solver.preprocess();
    solver.solve();
}





template <class GridType>
bool checkWithGrid(const GridType& grid, const std::string fileName="")
{
    bool passed = true;

    static const int dim = GridType::dimension;

    typedef typename Dune::FieldMatrix<double, 1, 1> MatrixBlock;
    typedef typename Dune::FieldVector<double, 1> VectorBlock;
    typedef typename Dune::BCRSMatrix<MatrixBlock> Matrix;
    typedef typename Dune::BlockVector<VectorBlock> Vector;
    typedef typename Dune::BitSetVector<1> BitVector;

    typedef typename GridType::LeafGridView GridView;
    typedef typename Dune::FieldVector<double, dim> DomainType;
    typedef typename Dune::FieldVector<double, 1> RangeType;


    const GridView gridView = grid.leafGridView();

    Matrix A;
    constructPQ1Pattern(gridView, A);
    A=0.0;
    assemblePQ1Stiffness(gridView, A);

    Vector rhs(A.N());
    rhs = 0;
    auto f = [](const DomainType& x) -> RangeType
      {return 50.0; };

    assemblePQ1RHS(gridView, rhs, f);

    Vector u(A.N());
    u = 0;

    BitVector ignore(A.N());
    ignore.unsetAll();
    markBoundaryDOFs(gridView, ignore);


    solveObstacleProblemByMMGSolver(grid, A, u, rhs, ignore);

    if (fileName!="")
    {
        typename Dune::VTKWriter<GridView> vtkWriter(gridView);
        vtkWriter.addVertexData(u, "solution");
        vtkWriter.write(fileName);
    }

    return passed;
}


template <int dim>
bool checkWithYaspGrid(int refine, const std::string fileName="")
{
    bool passed = true;

    typedef Dune::YaspGrid<dim> GridType;

    typename Dune::FieldVector<typename GridType::ctype,dim> L(1.0);
    typename std::array<int,dim> s;
    std::fill(s.begin(), s.end(), 1);

    GridType grid(L, s);

    for (int i = 0; i < refine; ++i)
        grid.globalRefine(1);

    std::cout << "Testing with YaspGrid<" << dim << ">" << std::endl;
    std::cout << "Number of levels: " << (grid.maxLevel()+1) << std::endl;
    std::cout << "Number of leaf nodes: " << grid.leafGridView().size(dim) << std::endl;

    passed = passed and checkWithGrid(grid, fileName);


    return passed;
}




int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);


    int refine1d = 10;
    int refine2d = 5;
    int refine3d = 3;

    if (argc>1)
        std::istringstream(argv[1]) >> refine1d;
    if (argc>2)
        std::istringstream(argv[2]) >> refine2d;
    if (argc>3)
        std::istringstream(argv[3]) >> refine3d;

    passed = passed and checkWithYaspGrid<1>(refine1d, "mmgtest_yasp_1d_solution");
    passed = passed and checkWithYaspGrid<2>(refine2d, "mmgtest_yasp_2d_solution");
    passed = passed and checkWithYaspGrid<3>(refine3d, "mmgtest_yasp_3d_solution");

    return passed ? 0 : 1;
}
