// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>
#include <iostream>
#include <sstream>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bcrsmatrix.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/cholmodsolver.hh>


#include "common.hh"

using namespace Dune;

/**  \brief test for the CholmodSolver class
  *
  *  This test tests whether the Dirichlet problem for a Laplace operator is solved correctly for a "random" rhs by an CholmodSolver.
  *  Implementation is more or less stolen from UMFPackSolverTest
  */
template <size_t blocksize>
struct CHOLMODSolverTestSuite
{
  template <class GridType>
  bool check(const GridType& grid)
  {
    double tol = 1e-12;

    bool passed = true;

    using Problem =
        SymmetricSampleProblem<blocksize, typename GridType::LeafGridView>;
    Problem p(grid.leafGridView());

    typedef Solvers::CholmodSolver<typename Problem::Matrix,
                                   typename Problem::Vector> Solver;
    Solver solver(p.A,p.u,p.rhs);
    solver.setIgnore(p.ignore);

    // solve problem
    solver.preprocess();
    solver.solve();

    if (p.energyNorm.diff(p.u,p.u_ex)>tol*10)
    {
      std::cout << "The CholmodSolver did not produce a satisfactory result. ||u-u_ex||=" << p.energyNorm.diff(p.u,p.u_ex) << std::endl;
      std::cout << "||u_ex||=" << p.energyNorm(p.u_ex) << std::endl;
      passed = false;
    }

    return passed;
  }
};



int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    bool passed = true;

    CHOLMODSolverTestSuite<1> testsuite1;
    CHOLMODSolverTestSuite<2> testsuite2;
    passed = checkWithStandardGrids(testsuite1);
    passed = checkWithStandardGrids(testsuite2);

    return passed ? 0 : 1;
}
