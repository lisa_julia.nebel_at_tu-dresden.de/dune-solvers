// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef NULLOPERATOR_HH
#define NULLOPERATOR_HH

#include <cstddef>

/** \brief Represents the null operator in the needed space
 *
 */
template <class BlockType>
class NullOperator
{
    private:
        /** \brief A dummy class for the rows
         *
         *  Contains a zero block to refer to
         */
        class RowDummy
        {
            private:
                BlockType zero_;

            public:
                RowDummy(): zero_(0){}

                const BlockType& operator[]([[maybe_unused]] size_t i) const
                {
                    return zero_;
                }
                BlockType& operator[]([[maybe_unused]] size_t i)
                {
                    return zero_;
                }
        };

        //! a dummy row
        RowDummy rowDummy_;

    public:
        //! export the block type
        typedef BlockType block_type;
        //! export the field type
        typedef typename block_type::field_type field_type;
        //! export the size type
        typedef size_t size_type;

        //! Default constructor
        NullOperator(){}

        /** \brief constructor taking anything
         *
         *  This is here to allow for NullOperator as block_type (this is needed in the constructor of RowDummy)
         */
//        template <class T>
//        NullOperator(const T& t){}

        /** \brief Matrix-Vector multiplication
         *
         *  Implements b += Nx and hence does nothing (N=0 !)
         */
        template <class LVectorType, class RVectorType>
        void umv([[maybe_unused]] const LVectorType& x, [[maybe_unused]] RVectorType& b) const
        {}

        /** \brief transposed Matrix-Vector multiplication
         *
         *  Implements b += N^tx and hence does nothing (N=0 !)
         */
        template <class LVectorType, class RVectorType>
        void umtv([[maybe_unused]] const LVectorType& x, [[maybe_unused]] RVectorType& b) const
        {}

        /** \brief Matrix-Vector multiplication with scalar multiplication
         *
         *  Implements b += a*Nx and hence does nothing (N=0 !)
         */
        template <class LVectorType, class RVectorType>
        void usmv([[maybe_unused]] const double a, [[maybe_unused]] const LVectorType& x, [[maybe_unused]] RVectorType& b) const
        {}

        /** \brief transposed Matrix-Vector multiplication with scalar multiplication
         *
         *  Implements b += a*N^tx and hence does nothing (N=0 !)
         */
        template <class LVectorType, class RVectorType>
        void usmtv([[maybe_unused]] const double a, [[maybe_unused]] const LVectorType& x, [[maybe_unused]] RVectorType& b) const
        {}

        /** \brief Matrix-Vector multiplication
         *
         *  Implements b = Nx and hence does nothing but set b=0 (N=0 !)
         */
        template <class LVectorType, class RVectorType>
        void mv([[maybe_unused]] const LVectorType& x, RVectorType& b) const
        {
            b = 0.0;
        }

        //! random access operator
        const RowDummy& operator[]([[maybe_unused]] size_t i) const
        {
            return rowDummy_;
        }

        //! random access operator
        RowDummy& operator[]([[maybe_unused]] size_t i)
        {
            return rowDummy_;
        }

        //! return j-th diagonal entry
        const block_type& diagonal([[maybe_unused]] size_t i) const
        {
            return rowDummy_[0];
        }

        //! multiplication operator with scalar
        void operator*=(const field_type&)
        {}

        size_type M() const
        {
            return 0;
        }

        size_type N() const
        {
            return 0;
        }


};

#endif

