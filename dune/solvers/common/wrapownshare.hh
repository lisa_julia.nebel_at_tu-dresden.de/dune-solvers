// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set ts=4 sw=2 et sts=2:
#ifndef DUNE_SOLVERS_COMMON_WRAP_OWN_SHARE_HH
#define DUNE_SOLVERS_COMMON_WRAP_OWN_SHARE_HH

#include <memory>
#include <algorithm>
#include <type_traits>

namespace Dune {

  namespace Solvers {

    namespace Impl {

      //! Wrap const reference as shared_ptr
      template<class T, class S>
      std::shared_ptr<const T> wrap_own_share(const S& t)
      {
        return std::shared_ptr<T>(&t, [](auto*){} );
      }

      //! Wrap reference as shared_ptr
      template<class T, class S>
      std::shared_ptr<T> wrap_own_share(S& t)
      {
        return std::shared_ptr<T>(&t, [](auto*){} );
      }

      //! Move r-value reference to shared_ptr
      template<class T, class S>
      std::shared_ptr<T> wrap_own_share(S&& t)
      {
        return std::make_shared<S>(std::move(t));
      }

      template<class T>
      std::shared_ptr<T> wrap_own_share(std::nullptr_t)
      {
        return {};
      }

      //! Share ownership of shared_ptr
      template<class T, class S>
      std::shared_ptr<T> wrap_own_share(std::shared_ptr<S> t)
      {
        return t;
      }

    } // end namespace Impl

    /** \brief Convert l-value and r-value references, and shared_ptr of an object into a shared_ptr of a convertible type.
     *
     * L-value references are wrapped, r-value references are moved and shared_ptr are shared.
     *
     * \tparam T The target type
     * \tparam S The input type
     * \param s An l-value or r-value reference, or shared_ptr of type S
     * \returns Shared pointer of the base class type.
     */
    template <class T, class S, class Enable = std::enable_if_t<not std::is_pointer<S>::value> >
    auto wrap_own_share(S&& s) {
      return Impl::wrap_own_share<T>(std::forward<S>(s));
    }

  } // end namespace Solvers

} // namespace Dune
#endif
