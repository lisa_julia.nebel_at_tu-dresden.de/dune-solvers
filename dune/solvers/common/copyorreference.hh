// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_SOLVERS_COMMON_COPYORREFERENCE_HH
#define DUNE_SOLVERS_COMMON_COPYORREFERENCE_HH

#include <dune/common/indices.hh>
#include <dune/common/concept.hh>

#include <dune/solvers/common/defaultbitvector.hh>



namespace Dune {
namespace Solvers {


/**
 * \brief A wrapper storing either a copy or reference
 *
 * \tparam T Type to be stored
 *
 * If T is of reference type, the wrapper stores a reference
 * to the passed object. Otherwise it stores a copy.
 *
 * Internally, for reference type T, this wrapper stores a pointer, which
 * results in CopyOrReference being copyable (etc.). This sets this wrapper
 * apart from having a pure member type T in a class in particular.
 * Furthermore, using this wrapper provides gclarity for the reader that type T
 * may be both, of raw type or of reference type.
 *
 * Usage of this wrapper comes at the price of get()-semantics.
 * As an alternative with pointer semantics consider using wrap_own_share but
 * mind the dynamic overhead.
 */
template<class T>
class CopyOrReference
{
public:
  using Type = std::decay_t<T>;

  constexpr CopyOrReference(const Type& other) :
    t_(other)
  {}

  constexpr CopyOrReference(Type&& other) :
    t_(std::move(other))
  {}

  constexpr const Type& get() const
  {
    return t_;
  }

  Type& get()
  {
    return t_;
  }

  constexpr std::true_type hasCopy() const
  {
    return {};
  }

private:
  Type t_;
};



template<class T>
class CopyOrReference<T&>
{
public:
  using Type = std::decay_t<T>;

  constexpr CopyOrReference(Type& other) :
    t_(&other)
  {}

  constexpr const Type& get() const
  {
    return *t_;
  }

  Type& get()
  {
    return *t_;
  }

  constexpr std::false_type hasCopy() const
  {
    return {};
  }

private:
  Type* t_;
};



template<class T>
class CopyOrReference<const T&>
{
public:
  using Type = std::decay_t<T>;

  constexpr CopyOrReference(const Type& other) :
    t_(&other)
  {}

  constexpr const Type& get() const
  {
    return *t_;
  }

  constexpr std::false_type hasCopy() const
  {
    return {};
  }

private:
  const Type* t_;
};



/**
 * \brief A wrapper storing either a const copy or const reference
 *
 * \tparam T Type to be stored
 *
 * This is an alias for CopyOrReference<TT> where TT
 * is either 'const T' if T is a raw type or 'const S&'
 * if T is a 'S&'. Notice that in the latter case this
 * is different from 'const T' which would be the same
 * as T.
 */
template<class T>
using ConstCopyOrReference =
  CopyOrReference<
    std::conditional_t<
      std::is_reference<T>::value,
      const std::remove_reference_t<T>&,
      const T>>;



} // end namespace Solvers
} // end namespace Dune



#endif // DUNE_SOLVERS_COMMON_COPYORREFERENCE_HH
