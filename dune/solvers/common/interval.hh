// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_COMMON_INTERVAL_HH
#define DUNE_SOLVERS_COMMON_INTERVAL_HH

#include <array>
#include <iostream>
#include <algorithm>

namespace Dune {

  namespace Solvers {

/** \brief Encapsulates a closed interval
 * \tparam field_type The type used for real numbers
 */
template <class field_type>
class Interval
{
public:
    /** \brief Default constructor */
    Interval()
    {}

    /** \brief Construct from an initializer list */
    Interval(std::initializer_list<field_type> const &input)
    {
        std::copy(input.begin(), input.end(), data_.begin());
    }

    /** \brief Construct from a scalar */
    Interval(const field_type& s)
    {
      data_[0] = data_[1] = s;
    }

    /** \brief Array-like access
     */
    field_type& operator[](int i)
    {
        return data_[i];
    }

    /** \brief Const array-like access
     */
    const field_type& operator[](int i) const
    {
        return data_[i];
    }

    /** \brief Addition
     */
    Interval& operator+=(const Interval& other)
    {
      data_[0] += other.data_[0];
      data_[1] += other.data_[1];
      return *this;
    }

    /** \brief Scalar multiplication
     */
    Interval& operator*=(const field_type& c)
    {
      data_[0] *= c;
      data_[1] *= c;

      // swap if multiplication with negative number
      if ( c < 0.0 )
        std::swap(data_[0],data_[1]);

      return *this;
    }

    /** \brief Project a scalar onto the interval
     */
    field_type projectIn(const field_type& x) const
    {
        // NB: C++17 has std::clamp(x, data_[0], data_[1]).
        //     Note, however, that we need to guarantee neither
        //     data_[0] nor data_[1] is NaN then.
        return std::max(std::min(x,data_[1]), data_[0]);
    }

    /** \brief Fast projection onto the interval if you know that your value
     *         is smaller than your upper bound
     */
    field_type projectFromBelow(const field_type& x) const
    {
        return std::max(x,data_[0]);
    };

    /** \brief Fast projection onto the interval if you know that your value
     *         is larger than your lower bound
     */
    field_type projectFromAbove(const field_type& x) const
    {
        return std::min(x,data_[1]);
    };

    /** \brief Return true if zero is contained in the interval
     * \param safety An additive safety distance
     */
    bool containsZero(const field_type& safety) const
    {
        return (data_[0] <= safety) and (-safety <= data_[1]);
    };

private:

    /** \brief The actual data */
    std::array<field_type,2> data_;
};


//! Scalar multiplication from the left
template <class field_type>
Interval<field_type> operator*(const field_type& c, const Interval<field_type>& i)
{
  auto ret = i;
  return ret *= c;
}

//! Scalar multiplication from the right
template <class field_type>
Interval<field_type> operator*(const Interval<field_type>& i, const field_type& c)
{
  return c * i;
}

//! Output operator for Interval
template <class field_type>
inline std::ostream& operator<< (std::ostream& s, const Interval<field_type>& interval)
{
    s << "[" << interval[0] << ", " << interval[1] << "]";
    return s;
}

  }   // namespace Solvers

}   // namespace Dune

#endif
