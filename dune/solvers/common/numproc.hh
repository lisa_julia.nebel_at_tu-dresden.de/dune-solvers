// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_SOLVERS_COMMON_NUMPROC_HH
#define DUNE_SOLVERS_COMMON_NUMPROC_HH

#include <dune/common/exceptions.hh>
#include <dune/common/parametertree.hh>

/** \brief Exception thrown by solvers */
class SolverError : public Dune::Exception {};

/** \brief Base class for numerical procedures */
class NumProc
{
public:
  /** \brief Different levels of verbosity */
  enum VerbosityMode {QUIET, REDUCED, FULL};

  /** \brief Constructor, per defaut uses FULL. */
  NumProc(VerbosityMode verbosity = FULL)
    : verbosity_(verbosity)
  {}

  /** \brief Construct from parameter file. */
  NumProc(const Dune::ParameterTree& config)
  {
    setVerbosity(config);
  }

  /** \brief Set verbosity from parameter file. */
  void setVerbosity(const Dune::ParameterTree& config) {
    verbosity_ = config.get<VerbosityMode>("verbosity");
  }

  /** \brief Set the verbosity level */
  void setVerbosity(VerbosityMode verbosity) { verbosity_ = verbosity; }

  /** \brief Get the verbosity level */
  const VerbosityMode& getVerbosity() const { return verbosity_; }

  /** \brief Check if verbosity is quiet. */
  bool isQuiet() const { return verbosity_ == QUIET; }

  /** \brief Check if verbosity is reduced. */
  bool isReduced() const { return verbosity_ == REDUCED; }

  /** \brief Check if verbosity is full. */
  bool isFull() const { return verbosity_ == FULL; }

protected:
  /** \brief Controls the amount of screen output of a numproc */
  VerbosityMode verbosity_;
};

inline
std::istream& operator>>(std::istream &lhs, NumProc::VerbosityMode &e)
{
  std::string s;
  lhs >> s;

  if (s == "full" || s == "2")
    e = NumProc::FULL;
  else if (s == "reduced" || s == "1")
    e = NumProc::REDUCED;
  else if (s == "quiet" || s == "0")
    e = NumProc::QUIET;
  else
    lhs.setstate(std::ios_base::failbit);

  return lhs;
}
#endif
