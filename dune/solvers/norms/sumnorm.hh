// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef SUMNORM_HH
#define SUMNORM_HH

#include <cmath>

#include "norm.hh"

template <class V>
class SumNorm: public Norm<V>
{
    public:
        typedef V VectorType;
        using Base = Norm<V>;

        /** \brief The type used for the result */
        using typename Base::field_type;

        SumNorm(field_type _alpha1, const Norm<VectorType> &_norm1, field_type _alpha2, const Norm<VectorType> &_norm2) :
            alpha1(_alpha1),
            norm1(_norm1),
            alpha2(_alpha2),
            norm2(_norm2)
    {}

        //! Compute the norm of the given vector
        field_type operator()(const VectorType &f) const override
        {
            return std::sqrt(normSquared(f));
        }

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const override
        {
            field_type r1 = norm1.normSquared(f);
            field_type r2 = norm2.normSquared(f);

            return alpha1 * r1 + alpha2 * r2;
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType &f1, const VectorType &f2) const override
        {
            field_type r1 = norm1.diff(f1,f2);
            field_type r2 = norm2.diff(f1,f2);

            return std::sqrt(alpha1 * r1 * r1 + alpha2 * r2 *r2);
        }

    private:
        const field_type alpha1;
        const Norm<VectorType> &norm1;

        const field_type alpha2;
        const Norm<VectorType> &norm2;


};

#endif
