// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_H1_SEMINORM_HH
#define DUNE_H1_SEMINORM_HH

#include <cmath>

#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/solvers/common/wrapownshare.hh>
#include "norm.hh"

//! Specialisation of the EnergyNorm class to identity blocks
template<class V, class M = Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >>
class H1SemiNorm : public Norm<V>
{
public:
    typedef V VectorType;
    using Base = Norm<V>;

    using MatrixType = M;

    /** \brief The type used for the result */
    using typename Base::field_type;

    H1SemiNorm() = default;

    template<typename Matrix>
    H1SemiNorm(Matrix&& matrix)
        : matrix_(Dune::Solvers::wrap_own_share<const MatrixType>(std::forward<Matrix>(matrix)))
    {
      assert(matrix_->N() == matrix_->M());
    }

    //! Compute the norm of the difference of two vectors
    field_type diff(const VectorType& u1, const VectorType& u2) const override
    {
          assert(u1.size()==u2.size());
          assert(u1.size()==matrix_->N());

    field_type sum = 0;

		for (size_t i=0; i<matrix_->N(); i++)
		{
      auto cIt    = (*matrix_)[i].begin();
      auto cEndIt  = (*matrix_)[i].end();

      typename VectorType::block_type differ_i = u1[i] - u2[i];
			for (; cIt!=cEndIt; ++cIt)
				sum += differ_i * (u1[cIt.index()]-u2[cIt.index()]) * (*cIt);
        }

        return std::sqrt(sum);
    }

    //! Compute the norm of the given vector
    field_type operator()(const VectorType& u) const override
    {
        if (matrix_ == NULL)
            DUNE_THROW(Dune::Exception, "You have not supplied neither a matrix nor an IterationStep to the EnergyNorm routine!");

        assert(u.size()==matrix_->N());

        // we compute sqrt{uAu^t}.  We have implemented this by hand because the matrix is
        // always scalar but the vectors may not be
        field_type sum = 0;

        for (size_t i=0; i<matrix_->N(); i++) {

            auto cIt    = (*matrix_)[i].begin();
            auto cEndIt = (*matrix_)[i].end();

            for (; cIt!=cEndIt; ++cIt)
                sum += u[i]*u[cIt.index()] * (*cIt);

        }

        return std::sqrt(sum);
    }

    std::shared_ptr<const MatrixType> matrix_;

};

#endif
