install(FILES
    blocknorm.hh
    diagnorm.hh
    energynorm.hh
    fullnorm.hh
    h1seminorm.hh
    norm.hh
    pnorm.hh
    reorderedblocknorm.hh
    sumnorm.hh
    twonorm.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/solvers/norms)
