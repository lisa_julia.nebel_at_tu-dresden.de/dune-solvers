# Master (will become release 2.8)

- `UMFPackSolver` can now handle matrices and vectors with scalar entries.

- CholmodSolver: Added `errorCode_` member to report errors and warnings during the matrix decomposition

- CholmodSolver: `CholmodSolver` can be used with any blocked matrix/vector type supported by `flatMatrixForEach` and `flatVectorForEach` in dune-istl.

## Deprecations and removals

- The file `blockgsstep.hh` has been removed after four years of deprecation.  Use the replacement
  in `blockgssteps.hh` instead.
